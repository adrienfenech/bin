#!/bin/bash
shopt -s xph_echo

while [ 1 ]
do
    feh -z --bg-fill ~/Images
    sleep 10
done

exit 0
