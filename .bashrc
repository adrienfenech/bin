#
# ~/.bashrc
#

# Export
export PATH=$PATH:$HOME/bin
export PATH=$PATH:$HOME/sbin
export PATH=$PATH:$HOME/android-studio/bin

# Add excaping's interpretation by backslash by defautl
shopt -s xpg_echo

# Alias for different script
alias sound='alsamixer'
alias android='./android-studio/bin/studio.sh & exit 0'
alias zl='i3lock -i ~/Images/space_sized_2.png'
alias l='~/bin/l.sh'
alias sfr='setxkbmap fr'
alias sus='setxkbmap us'
alias la='~/bin/l.sh a'
alias lt='echo -e "\033[32m "; tree; echo -e "\033[0m"'

# Alias for docker
alias dbash='docker exec -it'
alias dhelp='~/bin/dockerhelp.sh'
alias dwebrun= 'docker run -d -P'


# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1="\$? \$(if [[ \$? == 0 ]]; then echo \"\[\033[0;32m\];)\"; elif [[ \$? -ne 1 ]]; then echo \"\[\033[0;33m\];|\"; else echo \"\[\033[0;31m\];(\"; fi)\[\033[00m\] \e[0;36m\u \e[min\e[0;36m \W \e[mat\e[0;36m \t \$ \e[m"
