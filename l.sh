#!/bin/bash

shopt -s xpg_echo

echo -e '\033[32m -- Current Directory : \033[4m'$PWD'\033[0m'
echo -e '\033[1;31m ## Directory Status ## \033[0m'
if [ $# -eq 1 ] && [ $1 == "a" ]; then
ls -la | sed -e "s/root\|ROOT/$(echo -e '\033[31m'ROOT'\033[0m')/" -e "s/$USER/$(echo -e '\033[37m'$USER'\033[0m')/" -e "/\<.sh$\>/ s/.*/& $(echo -e '\033[1m'---'\033[0m' '\033[33m'Shell script'\033[0m')/" -e "/^-/ s/.*/& $(echo -e '\033[1m'---'\033[0m' '\033[36m'File'\033[0m')/" -e "/.*\.png/ s/.*/& $(echo -e '\033[1m'-'\033[0m' '\033[34m'Image'\033[0m')/" -e "/^d/ s/.*/& $(echo -e '\033[1m'---'\033[0m' '\033[32m'Directory'\033[0m')/" -e "/^l/ s/.*/& $(echo -e '\033[1m'---'\033[0m' '\033[35m'Symbolic Link'\033[0m')/" -e "/^p/ s/.*/& $(echo -e '\033[1m'---'\033[0m' '\033[34m'Named Pipe'\033[0m')/" -e "/^c\|^b/ s/.*/& $(echo -e '\033[1m'---'\033[0m' '\033[36m'Device File'\033[0m')/"
else
ls -l | sed -e "s/root\|ROOT/$(echo -e '\033[31m'ROOT'\033[0m')/" -e "s/$USER/$(echo -e '\033[37m'$USER'\033[0m')/" -e "/\<.sh$\>/ s/.*/& $(echo -e '\033[1m'---'\033[0m' '\033[33m'Shell script'\033[0m')/" -e "/^-/ s/.*/& $(echo -e '\033[1m'---'\033[0m' '\033[36m'File'\033[0m')/" -e "/.*\.png/ s/.*/& $(echo -e '\033[1m'-'\033[0m' '\033[34m'Image'\033[0m')/" -e "/^d/ s/.*/& $(echo -e '\033[1m'---'\033[0m' '\033[32m'Directory'\033[0m')/" -e "/^l/ s/.*/& $(echo -e '\033[1m'---'\033[0m' '\033[35m'Symbolic Link'\033[0m')/" -e "/^p/ s/.*/& $(echo -e '\033[1m'---'\033[0m' '\033[34m'Named Pipe'\033[0m')/" -e "/^c\|^b/ s/.*/& $(echo -e '\033[1m'---'\033[0m' '\033[36m'Device File'\033[0m')/"
fi


if [ -d ".git" ]; then
echo -e '\033[1;31m ## Git Status ## \033[0m'
git log -1 --oneline -1
git status --porcelain | sed -e "/A  / s/.*/& $(echo -e '\033[1m'--- '\033[0m\033[36m'ADDED'\033[0m')/" -e "/D  / s/.*/& $(echo -e '\033[1m'--- '\033[0m\033[33m'DELETED'\033[0m')/" -e "/ D / s/.*/& $(echo -e '\033[1m'--- '\033[0m\033[33m'DELETED'\033[0m')/" -e "/ M / s/.*/& $(echo -e '\033[1m'--- '\033[0m\033[31m'NOT UPDATED'\033[0m')/" -e "/M  / s/.*/& $(echo -e '\033[1m' --- '\033[0m\033[32m'UPDATED'\033[0m')/" -e "/?? / s/.*/& $(echo -e '\033[1m' --- '\033[0m\033[33m'UNTRACKED'\033[0m')/" -e "/\/[^\/]*\.[^\/ ]*/ s//$(echo -e '\033[47m'\& '\033[0m')/"
fi
#git status -s | sed -e "/\<^M  .*\>/ s/.*/& --- $(echo -e '\033[32m' MOD \\\/ ADDED'\033[0m')/" -e "/\<M\s\s.*\>/ s/.*/& --- $(echo -e '\033[31m' MOD \\\/ NOT ADDED'\033[0m')/" -e "/\<?? .*\>/ s/.*/& --- $(echo -e '\033[31m' NEW \\\/ NOT ADDED'\033[0m')/"
