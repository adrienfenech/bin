#!/bin/bash
shopt -s xpg_echo

# This script is a snake like
# Version 0.6

# Variables

colorp1='\033[0;31m'
colorp2='\033[0;33m'
colorp3='\033[0;36m'
colorp4='\033[0;34m'
colorSuccess='\033[0;32m'
colorSpecial='\033[0;35m'
colorNormal='\033[0;36m'
colorErr='\033[0;31m'
null='\033[0m'

col=`tput cols`
lin=`tput lines`
colx=$(($col-1))
linx=$(($lin-1))
colxx=$(($col-2))
linxx=$(($lin-2))

pos1X=1
pos1Y=4
lat1=0

pos2X=$colxx
pos2Y=4
lat2=0

pos3X=1
pos3Y=$linxx
lat3=0

pos4X=$colxx
pos4Y=$linxx
lat4=0

count=0

itemPosX=0
itemPosY=0
itemPosNextX=0
itemPosNextY=0
itemStartX=0
itemStartY=0
itemFinishX=0
itemFinishY=0
effectPosX=0
effectPosY=0

mode=0
rapidity=0
team=0
wall=0
effect=0
countEffect=10
latEffect=0

p1=$1
p2=$2
p3=$3
p4=$4

point1=0
point2=0
point3=0
point4=0

score=10
ti=0.01
speed=0.01

tmp=""
final=0
read tmp
while [ $final -eq 0 ]
do
    echo $tmp
    read tmp

    echo $tmp | grep '# [a-zA-Z0-9]*'
    if [ $? -eq 0 ]; then echo 'comment'
    elif [ $tmp == "#Player1" ]; then read tmp; p1=$tmp
    elif [ $tmp == "#Player2" ]; then read tmp; p2=$tmp
    elif [ $tmp == "#Player3" ]; then read tmp; p3=$tmp
    elif [ $tmp == "#Player4" ]; then read tmp; p4=$tmp
    elif [ $tmp == "#Speed" ]
    then
        read tmp
        if [ $tmp == "x1" ]
        then
            ti=0.01
            speed=$tmp
        elif [ $tmp == "x2" ]
        then
            ti=0.005
            speed=$tmp
        elif [ $tmp == "x5" ]
        then
            ti=0.001
            speed=$tmp
        else
            ti=0.01
            speed=$tmp
        fi
    elif [ $tmp == "#Team" ]; then read tmp; team=$tmp
    elif [ $tmp == "#Effect" ]; then read tmp; effect=$tmp
    elif [ $tmp == "#Wall" ]; then read tmp; wall=$tmp
    elif [ $tmp == "#Score_goal" ]; then read tmp; score=$(($tmp-1))
    elif [ $tmp == "#Game_mode" ]
    then
        read tmp
        if [ $tmp == "run" ]
        then
            mode=1
        elif [ $tmp == "rapidity" ]
        then
            mode=2
        elif [ $tmp == "runity" ]
        then
            mode=3
        elif [ $tmp == "cheetah" ]
        then
            mode=4
        elif [ $tmp == "miam" ]
        then
            mode=5
        elif [ $tmp == "normal" ]
        then
            mode=0
        fi
    elif [ $tmp == "#EOF" ]
    then
        final=1
    fi
done

if [ $team -eq 1 ]
then
    colorp1='\033[0;34m'
    colorp2='\033[0;32m'
    colorp3='\033[0;32m'
    colorp4='\033[0;34m'
fi

# function

fun_approx_distance()
{
    x1=$1
    y1=$2
    x2=$3
    y2=$4
    xx=$(($x2-$x1))
    yy=$(($y2-$y1))
    xx=$(($xx*$xx))
    yy=$(($yy*$yy))
    approx=$(($xx+$yy))

    echo $approx
}

fun_random()
{
    rand=-1
    while [ $rand -lt $2 ]
    do
        rand=$RANDOM
        let "rand %= $1"
    done
    echo $rand
}

fun_top_bar()
{
    posXtmp=$col
    posXtmp=$(($posXtmp-14))
    posXXtmp=$(($posXtmp-17))
    tput cup 0 0
    while [ $count -lt $col ]
    do
        echo -n '-'
        count=$(($count+1))
    done
    count=0
    tput cup 1 0
    tput el
    tput el1
    if [ $team -eq 0 ]
    then
        echo -n $colorp1$p1$null' : '$colorp1$point1$null' | '$colorp2$p2$null' : '$colorp2$point2$null' | '$colorp3$p3$null' : '$colorp3$point3$null' | '$colorp4$p4$null' : '$colorp4$point4$null
    else
        pointTeam1=$(($point1+$point4))
        pointTeam2=$(($point2+$point3))
        echo -n $colorp1$p1$p4$null' : '$colorp1$pointTeam1$null' | '$colorp2$p2$p3$null' : '$colorp2$pointTeam2$null
    fi
    tput cup 1 $posXtmp
    echo -n ' | opt : '$colorSuccess$(($score+1))$null'|'$colorSuccess$speed$null
    tput cup 2 0
    while [ $count -lt $col ]
    do
        echo -n '-'
        count=$(($count+1))
    done
    count=0
}

fun_rapidity_bar()
{
    tput sc
    tput cup 1 $posXXtmp
    if [ $rapidity -gt 10 ]
    then
        echo -n ' | rapidity : '$colorSuccess$(($rapidity/10))$null
    else
        echo -n ' | rapidity : '$colorErr$(($rapidity/10))$null
    fi
    tput rc
}

fun_game_map()
{
    tput cup 3 0
    while [ $count -lt $col ]
    do
        echo -n $colorErr'-'$null
        count=$(($count+1))
    done
    count=0

    count=4
    while [ $count -lt $lin ]
    do
        tput cup $count 0
        echo -n $colorErr'|'$null
        count=$(($count+1))
    done
    count=0

    while [ $count -lt $colx ]
    do
        echo -n $colorErr'-'$null
        count=$(($count+1))
    done
    count=0

    count=$linx
    while [ $count -gt 3 ]
    do
        tput cup $count $col
        echo -n $colorErr'|'$null
        count=$(($count-1))
    done
    count=0

    if [ $wall -eq 1 ]
    then
        cnt=$(($colxx*$linxx))
        while [ $cnt -gt 0 ]
        do
            tab[$cnt]=' '
            cnt=$(($cnt-1))
        done
        cnt=$(($colxx*$linxx/50))
        while [ $cnt -gt 0 ]
        do
            tempX=$(fun_random $colx 1)
            tempY=$(fun_random $linx 4)
            tput cup $tempY $tempX
            tab[$(($colxx*$tempY+$tempX))]='O'
            #tput dch 1
            echo -n '\033[1mO\033[0m'
            cnt=$(($cnt-1))
        done
    fi
}

# UP
fun_up()
{
    if [ $1 -eq 1 ]
    then
        if [ $lat1 -eq 0 ]
        then
            fun_erase_last_1
            tput cup $pos1Y $pos1X
            tput cuu 1
            pos1Y=$(($pos1Y-1))
            echo -n $colorp1$p1$null
            tput cub 1
            lat1=1
        else
            lat1=0
        fi
    elif [ $1 -eq 2 ]
    then
        if [ $lat2 -eq 0 ]
        then
            fun_erase_last_2
            tput cup $pos2Y $pos2X
            tput cuu 1
            pos2Y=$(($pos2Y-1))
            echo -n $colorp2$p2$null
            tput cub 1
            lat2=1
        else
            lat2=0
        fi
    elif [ $1 -eq 3 ]
    then
        if [ $lat3 -eq 0 ]
        then
            fun_erase_last_3
            tput cup $pos3Y $pos3X
            tput cuu 1
            pos3Y=$(($pos3Y-1))
            echo -n $colorp3$p3$null
            tput cub 1
            lat3=1
        else
            lat3=0
        fi
    elif [ $1 -eq 4 ]
    then
        if [ $lat4 -eq 0 ]
        then
            fun_erase_last_4
            tput cup $pos4Y $pos4X
            tput cuu 1
            pos4Y=$(($pos4Y-1))
            echo -n $colorp4$p4$null
            tput cub 1
            lat4=1
        else
            lat4=0
        fi
    elif [ $1 -eq 5 ]
    then
        if [ $lat5 -eq 1 ] && [ $mode -eq 4 ] || [ $mode -eq 5 ]
        then
            fun_erase_last_5
            tput cup $itemPosY $itemPosX
            tput cuu 1
            itemPosY=$(($itemPosY-1))
            echo -n $colorSpecial'#'$null
            tput cub 1
            lat5=0
        elif [ $lat5 -eq 0 ] && [ $mode -eq 4 ] || [ $mode -eq 5 ]
        then
            lat5=1
        elif [ $lat5 -eq 3 ]
        then
            fun_erase_last_5
            tput cup $itemPosY $itemPosX
            tput cuu 1
            itemPosY=$(($itemPosY-1))
            echo -n $colorSpecial'#'$null
            tput cub 1
            lat5=0
        else
            lat5=$(($lat5+1))
        fi
    fi
}

# DOWN
fun_down()
{
    if [ $1 -eq 1 ]
    then
        if [ $lat1 -eq 0 ]
        then
            fun_erase_last_1
            tput cup $pos1Y $pos1X
            tput cud 1
            pos1Y=$(($pos1Y+1))
            echo -n $colorp1$p1$null
            tput cub 1
            lat1=1
        else
            lat1=0
        fi
    elif [ $1 -eq 2 ]
    then
        if [ $lat2 -eq 0 ]
        then
            fun_erase_last_2
            tput cup $pos2Y $pos2X
            tput cud 1
            pos2Y=$(($pos2Y+1))
            echo -n $colorp2$p2$null
            tput cub 1
            lat2=1
        else
            lat2=0
        fi
    elif [ $1 -eq 3 ]
    then
        if [ $lat3 -eq 0 ]
        then
            fun_erase_last_3
            tput cup $pos3Y $pos3X
            tput cud 1
            pos3Y=$(($pos3Y+1))
            echo -n $colorp3$p3$null
            tput cub 1
            lat3=1
        else
            lat3=0
        fi
    elif [ $1 -eq 4 ]
    then
        if [ $lat4 -eq 0 ]
        then
            fun_erase_last_4
            tput cup $pos4Y $pos4X
            tput cud 1
            pos4Y=$(($pos4Y+1))
            echo -n $colorp4$p4$null
            tput cub 1
            lat4=1
        else
            lat4=0
        fi
    elif [ $1 -eq 5 ]
    then
        if [ $lat5 -eq 1 ] && [ $mode -eq 4 ] || [ $mode -eq 5 ]
        then
            fun_erase_last_5
            tput cup $itemPosY $itemPosX
            tput cud 1
            itemPosY=$(($itemPosY+1))
            echo -n $colorSpecial'#'$null
            tput cub 1
            lat5=0
        elif [ $lat5 -eq 0 ] && [ $mode -eq 4 ] || [ $mode -eq 5 ]
        then
            lat5=1
        elif [ $lat5 -eq 3 ]
        then
            fun_erase_last_5
            tput cup $itemPosY $itemPosX
            tput cud 1
            itemPosY=$(($itemPosY+1))
            echo -n $colorSpecial'#'$null
            tput cub 1
            lat5=0
        else
            lat5=$(($lat5+1))
        fi
    fi
}

# LEFT
fun_left()
{
    if [ $1 -eq 1 ]
    then
        fun_erase_last_1
        tput cup $pos1Y $pos1X
        tput cub 1
        pos1X=$(($pos1X-1))
        echo -n $colorp1$p1$null
        tput cub 1
    elif [ $1 -eq 2 ]
    then
        fun_erase_last_2
        tput cup $pos2Y $pos2X
        tput cub 1
        pos2X=$(($pos2X-1))
        echo -n $colorp2$p2$null
        tput cub 1
    elif [ $1 -eq 3 ]
    then
        fun_erase_last_3
        tput cup $pos3Y $pos3X
        tput cub 1
        pos3X=$(($pos3X-1))
        echo -n $colorp3$p3$null
        tput cub 1
    elif [ $1 -eq 4 ]
    then
        fun_erase_last_4
        tput cup $pos4Y $pos4X
        tput cub 1
        pos4X=$(($pos4X-1))
        echo -n $colorp4$p4$null
        tput cub 1
    elif [ $1 -eq 5 ]
    then
        if [ $lat5 -eq 1 ] || [ $mode -eq 4 ] || [ $mode -eq 5 ]
        then
            fun_erase_last_5
            tput cup $itemPosY $itemPosX
            tput cub 1
            itemPosX=$(($itemPosX-1))
            echo -n $colorSpecial'#'$null
            tput cub 1
            lat5=0
        else
            lat5=1
        fi
    fi
}

# DOWN
fun_right()
{
    if [ $1 -eq 1 ]
    then
        fun_erase_last_1
        tput cup $pos1Y $pos1X
        tput cuf 1
        pos1X=$(($pos1X+1))
        echo -n $colorp1$p1$null
        tput cub 1
    elif [ $1 -eq 2 ]
    then
        fun_erase_last_2
        tput cup $pos2Y $pos2X
        tput cuf 1
        pos2X=$(($pos2X+1))
        echo -n $colorp2$p2$null
        tput cub 1
    elif [ $1 -eq 3 ]
    then
        fun_erase_last_3
        tput cup $pos3Y $pos3X
        tput cuf 1
        pos3X=$(($pos3X+1))
        echo -n $colorp3$p3$null
        tput cub 1
    elif [ $1 -eq 4 ]
    then
        fun_erase_last_4
        tput cup $pos4Y $pos4X
        tput cuf 1
        pos4X=$(($pos4X+1))
        echo -n $colorp4$p4$null
        tput cub 1
    elif [ $1 -eq 5 ]
    then
        if [ $lat5 -eq 1 ] || [ $mode -eq 4 ] || [ $mode -eq 5 ]
        then
            fun_erase_last_5
            tput cup $itemPosY $itemPosX
            tput cuf 1
            itemPosX=$(($itemPosX+1))
            echo -n $colorSpecial'#'$null
            tput cub 1
            lat5=0
        else
            lat5=1
        fi
    fi
}

# START
fun_start()
{
    if [ $mode -ne 5 ]
    then
        pos1X=1
        pos1Y=4
        tput cup $pos1Y $pos1X
        echo -n $colorp1$p1$null
        lat1=0

        pos2X=$colxx
        pos2Y=4
        tput cup $pos2Y $pos2X
        echo -n $colorp2$p2$null
        lat2=0

        pos3X=1
        pos3Y=$linxx
        tput cup $pos3Y $pos3X
        echo -n $colorp3$p3$null
        lat3=0

        pos4X=$colxx
        pos4Y=$linxx
        tput cup $pos4Y $pos4X
        echo -n $colorp4$p4$null
        lat4=0
    else
        pos1X=$(($colxx/2-1))
        pos1Y=$(($linxx/2-1))
        tput cup $pos1Y $pos1X
        echo -n $colorp1$p1$null
        lat1=0

        pos2X=$(($colxx/2+1))
        pos2Y=$(($linxx/2-1))
        tput cup $pos2Y $pos2X
        echo -n $colorp2$p2$null
        lat2=0

        pos3X=$(($colxx/2-1))
        pos3Y=$(($linxx/2+1))
        tput cup $pos3Y $pos3X
        echo -n $colorp3$p3$null
        lat3=0

        pos4X=$(($colxx/2+1))
        pos4Y=$(($linxx/2+1))
        tput cup $pos4Y $pos4X
        echo -n $colorp4$p4$null
        lat4=0
    fi

    count=0
    effectPosX=0
    effectPosY=0
    countEffect=10
    latEffect=0

    itemPosX=0
    itemPosY=0
    itemPosNextX=0
    itemPosNextY=0
    lat5=0

    point1=0
    point2=0
    point3=0
    point4=0
    fun_new_miam
}

fun_erase_last_1()
{
    tput cup $pos1Y $pos1X
    tput dch 1
    tput ich 1
}

fun_erase_last_2()
{
    tput cup $pos2Y $pos2X
    tput dch 1
    tput ich 1
}

fun_erase_last_3()
{
    tput cup $pos3Y $pos3X
    tput dch 1
    tput ich 1
}

fun_erase_last_4()
{
    tput cup $pos4Y $pos4X
    tput dch 1
    tput ich 1
}

fun_erase_last_5()
{
    tput cup $itemPosY $itemPosX
    tput dch 1
    tput ich 1
}

fun_erase_start()
{
    tput sc
    tput cup $itemStartY $itemStartX
    tput dch 1
    tput ich 1
    tput rc
}

fun_erase_finish()
{
    tput sc
    tput cup $itemFinishY $itemFinishX
    tput dch 1
    tput ich 1
    tput rc
}

fun_who_is_close_to_me()
{
    dist1=$(fun_approx_distance $pos1X $pos1Y $itemPosX $itemPosY)
    dist2=$(fun_approx_distance $pos2X $pos2Y $itemPosX $itemPosY)
    dist3=$(fun_approx_distance $pos3X $pos3Y $itemPosX $itemPosY)
    dist4=$(fun_approx_distance $pos4X $pos4Y $itemPosX $itemPosY)
    who=$dist1
    temp=0
    while [ $who -ne $temp ]
    do
        temp=$who
        if [ $who -gt $dist1 ]; then who=$dist1
        elif [ $who -gt $dist2 ]; then who=$dist2
        elif [ $who -gt $dist3 ]; then who=$dist3
        elif [ $who -gt $dist4 ]; then who=$dist4
        fi
    done
    if [ $who -eq $dist1 ]
    then
        itemPosNextX=$pos1X
        itemPosNextY=$pos1Y
    elif [ $who -eq $dist2 ]
    then
        itemPosNextX=$pos2X
        itemPosNextY=$pos2Y
    elif [ $who -eq $dist3 ]
    then
        itemPosNextX=$pos3X
        itemPosNextY=$pos3Y
    elif [ $who -eq $dist4 ]
    then
        itemPosNextX=$pos4X
        itemPosNextY=$pos4Y
    fi
}

fun_new_miam_destination()
{
    tput sc
    itemPosNextX=$(fun_random $colx 1)
    itemPosNextY=$(fun_random $linx 4)
    fun_erase_finish
    itemFinishX=$itemPosNextX
    itemFinishY=$itemPosNextY
    tput sc
    tput cup $itemFinishY $itemFinishX
    echo -n $colorErr'.'$null
    tput rc
    tput rc
}

fun_effect_explosion()
{
    tput sc
    tput cup $(($effectPosY-$countEffect+1)) $(($effectPosX))
    tput dch 1
    tput ich 1

    tput cup $(($effectPosY+$countEffect-1)) $(($effectPosX))
    tput dch 1
    tput ich 1

    tput cup $(($effectPosY)) $(($effectPosX-$countEffect+1))
    tput dch 1
    tput ich 1

    tput cup $(($effectPosY)) $(($effectPosX+$countEffect-1))
    tput dch 1
    tput ich 1

    if [ $1 -eq 1 ]
    then
        echo $colorWin
        tput cup $(($effectPosY-$countEffect)) $(($effectPosX))
        echo -n $colorWin'|'
        tput cup $(($effectPosY+$countEffect)) $(($effectPosX))
        echo -n $colorWin'|'
        tput cup $(($effectPosY)) $(($effectPosX-$countEffect))
        echo -n $colorWin'-'
        tput cup $(($effectPosY)) $(($effectPosX+$countEffect))
        echo -n $colorWin'-'$null
    fi
    tput rc
}

fun_new_miam()
{
    if [ $countEffect -ne 0 ] && [ $latEffect -ne 0 ] && [ $effectPosX -ne 0 ]
    then
        fun_effect_explosion 0
    fi
    if [ $latEffect -ne 0 ]
    then
        countEffect=0
        effectPosX=$itemPosX
        effectPosY=$itemPosY
    fi
    latEffect=1
    tput sc
    rapidity=39
    itemPosX=$(fun_random $colx 1)
    itemPosY=$(fun_random $linx 4)
    tput cup $itemPosY $itemPosX
    echo -n $colorSpecial'#'$null
    if [ $mode -eq 3 ]
    then
        fun_erase_start
        itemStartX=$itemPosX
        itemStartY=$itemPosY
        tput sc
        tput cup $itemStartY $itemStartX
        echo -n $colorErr'.'$null
        tput rc
    fi
    tput rc
    fun_top_bar
    if [ $mode -eq 1 ] || [ $mode -eq 3 ] || [ $mode -eq 4 ]
    then
        fun_new_miam_destination
    elif [ $mode -eq 5 ]
    then
        fun_who_is_close_to_me
    fi
}

fun_time()
{
    tput cup $itemPosY $itemPosX
    tput dch 1
    tput ich 1
    fun_erase_last_1
    fun_erase_last_2
    fun_erase_last_3
    fun_erase_last_4
    fun_start
    tm=$1
    tput cup 1 34
    echo -n 'Next game in '$2$1$null' seconds'
    tput cub 9
    while [ $tm -gt 0 ]
    do
        sleep 1
        tm=$(($tm-1))
        echo -n  $2$tm$null
        tput cub 1
    done
}

fun_first_game()
{
    midX=$(($col/2-20))
    midY=$(($lin/2))
    fun_start
    tm=$1
    tput cup 1 32
    echo -n 'Next game in '$2$1$null' seconds'
    tput cub 9
    while [ $tm -gt 0 ]
    do
        sleep 1
        tm=$(($tm-1))
        echo -n  $2$tm$null
        tput cub 1
    done
}

fun_run_away_1()
{
    if [ $pos1X -lt $(($colxx)) ]
    then
        dist1=$(fun_approx_distance $(($pos1X+1)) $pos1Y $itemPosX $itemPosY)
    else
        dist1=0
    fi
    if [ $pos1X -gt 1 ]
    then
        dist2=$(fun_approx_distance $(($pos1X-1)) $pos1Y $itemPosX $itemPosY)
    else
        dist2=0
    fi
    if [ $pos1Y -lt $(($linxx)) ]
    then
        dist3=$(fun_approx_distance $pos1X $(($pos1Y+1)) $itemPosX $itemPosY)
    else
        dist3=0
    fi
    if [ $pos1Y -gt 4 ]
    then
        dist4=$(fun_approx_distance $pos1X $(($pos1Y-1)) $itemPosX $itemPosY)
    else
        dist4=0
    fi
    dist5=$(fun_approx_distance $pos1X $pos1Y $itemPosX $itemPosY)
    who=3
    temp=4
    temp2=5
    while [ $who -ne $temp ] || [ $temp -ne $temp2 ]
    do
        temp2=$temp
        temp=$who
        if [ $who -lt $dist1 ] && [ $dist1 -ne 0 ]; then who=$dist1
        elif [ $who -lt $dist2 ] && [ $dist2 -ne 0 ]; then who=$dist2
        elif [ $who -lt $dist3 ] && [ $dist3 -ne 0 ]; then who=$dist3
        elif [ $who -lt $dist4 ] && [ $dist4 -ne 0 ]; then who=$dist4
        elif [ $who -lt $dist5 ] && [ $dist5 -ne 0 ]; then who=$dist5
        fi
    done
    if [ $who -eq $dist1 ]
    then
        fun_right 1
    elif [ $who -eq $dist2 ]
    then
        fun_left 1
    elif [ $who -eq $dist3 ]
    then
        fun_down 1
    elif [ $who -eq $dist4 ]
    then
        fun_up 1
    fi
}

fun_run_away_2()
{
    if [ $pos2X -lt $(($colxx)) ]
    then
        dist1=$(fun_approx_distance $(($pos2X+1)) $pos2Y $itemPosX $itemPosY)
    else
        dist1=0
    fi
    if [ $pos2X -gt 1 ]
    then
        dist2=$(fun_approx_distance $(($pos2X-1)) $pos2Y $itemPosX $itemPosY)
    else
        dist2=0
    fi
    if [ $pos2Y -lt $(($linxx)) ]
    then
        dist3=$(fun_approx_distance $pos2X $(($pos2Y+1)) $itemPosX $itemPosY)
    else
        dist3=0
    fi
    if [ $pos2Y -gt 4 ]
    then
        dist4=$(fun_approx_distance $pos2X $(($pos2Y-1)) $itemPosX $itemPosY)
    else
        dist4=0
    fi
    dist5=$(fun_approx_distance $pos2X $pos2Y $itemPosX $itemPosY)
    who=3
    temp=4
    temp2=5
    while [ $who -ne $temp ] || [ $temp -ne $temp2 ]
    do
        temp2=$temp
        temp=$who
        if [ $who -lt $dist1 ] && [ $dist1 -ne 0 ]; then who=$dist1
        elif [ $who -lt $dist2 ] && [ $dist2 -ne 0 ]; then who=$dist2
        elif [ $who -lt $dist3 ] && [ $dist3 -ne 0 ]; then who=$dist3
        elif [ $who -lt $dist4 ] && [ $dist4 -ne 0 ]; then who=$dist4
        elif [ $who -lt $dist5 ] && [ $dist5 -ne 0 ]; then who=$dist5
        fi
    done
    if [ $who -eq $dist1 ]
    then
        fun_right 2
    elif [ $who -eq $dist2 ]
    then
        fun_left 2
    elif [ $who -eq $dist3 ]
    then
        fun_down 2
    elif [ $who -eq $dist4 ]
    then
        fun_up 2
    fi
}

fun_run_away_3()
{
    if [ $pos3X -lt $(($colxx)) ]
    then
        dist1=$(fun_approx_distance $(($pos3X+1)) $pos3Y $itemPosX $itemPosY)
    else
        dist1=0
    fi
    if [ $pos3X -gt 1 ]
    then
        dist2=$(fun_approx_distance $(($pos3X-1)) $pos3Y $itemPosX $itemPosY)
    else
        dist2=0
    fi
    if [ $pos3Y -lt $(($linxx)) ]
    then
        dist3=$(fun_approx_distance $pos3X $(($pos3Y+1)) $itemPosX $itemPosY)
    else
        dist3=0
    fi
    if [ $pos3Y -gt 4 ]
    then
        dist4=$(fun_approx_distance $pos3X $(($pos3Y-1)) $itemPosX $itemPosY)
    else
        dist4=0
    fi
    dist5=$(fun_approx_distance $pos3X $pos3Y $itemPosX $itemPosY)
    who=3
    temp=4
    temp2=5
    while [ $who -ne $temp ] || [ $temp -ne $temp2 ]
    do
        temp2=$temp
        temp=$who
        if [ $who -lt $dist1 ] && [ $dist1 -ne 0 ]; then who=$dist1
        elif [ $who -lt $dist2 ] && [ $dist2 -ne 0 ]; then who=$dist2
        elif [ $who -lt $dist3 ] && [ $dist3 -ne 0 ]; then who=$dist3
        elif [ $who -lt $dist4 ] && [ $dist4 -ne 0 ]; then who=$dist4
        elif [ $who -lt $dist5 ] && [ $dist5 -ne 0 ]; then who=$dist5
        fi
    done
    if [ $who -eq $dist1 ]
    then
        fun_right 3
    elif [ $who -eq $dist2 ]
    then
        fun_left 3
    elif [ $who -eq $dist3 ]
    then
        fun_down 3
    elif [ $who -eq $dist4 ]
    then
        fun_up 3
    fi
}

fun_run_away_4()
{
    if [ $pos4X -lt $(($colxx)) ]
    then
        dist1=$(fun_approx_distance $(($pos4X+1)) $pos4Y $itemPosX $itemPosY)
    else
        dist1=0
    fi
    if [ $pos4X -gt 1 ]
    then
        dist2=$(fun_approx_distance $(($pos4X-1)) $pos4Y $itemPosX $itemPosY)
    else
        dist2=0
    fi
    if [ $pos4Y -lt $(($linxx)) ]
    then
        dist3=$(fun_approx_distance $pos4X $(($pos4Y+1)) $itemPosX $itemPosY)
    else
        dist3=0
    fi
    if [ $pos4Y -gt 4 ]
    then
        dist4=$(fun_approx_distance $pos4X $(($pos4Y-1)) $itemPosX $itemPosY)
    else
        dist4=0
    fi
    dist5=$(fun_approx_distance $pos4X $pos4Y $itemPosX $itemPosY)
    who=3
    temp=4
    temp2=5
    while [ $who -ne $temp ] || [ $temp -ne $temp2 ]
    do
        temp2=$temp
        temp=$who
        if [ $who -lt $dist1 ] && [ $dist1 -ne 0 ]; then who=$dist1
        elif [ $who -lt $dist2 ] && [ $dist2 -ne 0 ]; then who=$dist2
        elif [ $who -lt $dist3 ] && [ $dist3 -ne 0 ]; then who=$dist3
        elif [ $who -lt $dist4 ] && [ $dist4 -ne 0 ]; then who=$dist4
        elif [ $who -lt $dist5 ] && [ $dist5 -ne 0 ]; then who=$dist5
        fi
    done
    if [ $who -eq $dist1 ]
    then
        fun_right 4
    elif [ $who -eq $dist2 ]
    then
        fun_left 4
    elif [ $who -eq $dist3 ]
    then
        fun_down 4
    elif [ $who -eq $dist4 ]
    then
        fun_up 4
    fi
}

fun_run_for_miam()
{
    if [ $itemPosX -lt $(($colxx)) ]
    then
        dist1=$(fun_approx_distance $(($itemNextPosX+1)) $itemNextPosY $itemPosX $itemPosY)
    else
        dist1=0
    fi
    if [ $itemPosX -gt 1 ]
    then
        dist2=$(fun_approx_distance $(($itemNextPosX-1)) $itemNextPosY $itemPosX $itemPosY)
    else
        dist2=0
    fi
    if [ $itemPosY -lt $(($linxx)) ]
    then
        dist3=$(fun_approx_distance $itemNextPosX $(($itemNextPosY+1)) $itemPosX $itemPosY)
    else
        dist3=0
    fi
    if [ $itemPosY -gt 4 ]
    then
        dist4=$(fun_approx_distance $itemNextPosX $(($itemNextPosY-1)) $itemPosX $itemPosY)
    else
        dist4=0
    fi
    dist5=$(fun_approx_distance $itemNextPosX $itemNextPosY $itemPosX $itemPosY)
    who=30000000
    temp=4
    temp2=5
    while [ $who -ne $temp ] || [ $temp -ne $temp2 ]
    do
        temp2=$temp
        temp=$who
        if [ $who -gt $dist1 ] && [ $dist1 -ne 0 ]; then who=$dist1
        elif [ $who -gt $dist2 ] && [ $dist2 -ne 0 ]; then who=$dist2
        elif [ $who -gt $dist3 ] && [ $dist3 -ne 0 ]; then who=$dist3
        elif [ $who -gt $dist4 ] && [ $dist4 -ne 0 ]; then who=$dist4
        elif [ $who -gt $dist5 ] && [ $dist5 -ne 0 ]; then who=$dist5
        fi
    done
    if [ $who -eq $dist1 ]
    then
        tput sc
        tput cup 3 40
        echo 'right'
        tput rc
        fun_right 5
    elif [ $who -eq $dist2 ]
    then
        tput sc
        tput cup 3 40
        echo 'left-'
        tput rc
        fun_left 5
    elif [ $who -eq $dist3 ]
    then
        tput sc
        tput cup 3 40
        echo 'down-'
        tput rc
        fun_down 5
    elif [ $who -eq $dist4 ]
    then
        tput sc
        tput cup 3 40
        echo 'up---'
        tput rc
        fun_up 5
    fi
}

clear
tput civis
fun_top_bar
fun_game_map
fun_first_game 5 $colorp1
while [ 1 ]
do
    sleep $ti

    if [ $mode -eq 1 ] || [ $mode -eq 3 ] || [ $mode -eq 4 ] || [ $mode -eq 5 ]
    then
        if [ $mode -eq 5 ]; then fun_who_is_close_to_me; fi
        if [ $mode -eq 3 ]
        then
            fun_rapidity_bar
            rapidity=$(($rapidity-1))
            if [ $rapidity -lt 0 ]
            then
                fun_erase_last_5
                fun_new_miam
                rapidity=39
            fi
        fi
        #fun_run_for_miam
        if [ $itemPosX -lt $itemPosNextX ]; then fun_right 5
        elif [ $itemPosY -gt $itemPosNextY ]; then fun_up 5
        elif [ $itemPosX -gt $itemPosNextX ]; then fun_left 5
        elif [ $itemPosY -lt $itemPosNextY ]; then fun_down 5
        elif [ $mode -ne 5 ]; then fun_new_miam_destination
        else fun_new_miam
        fi
    elif [ $mode -eq 2 ]
    then
        fun_rapidity_bar
        rapidity=$(($rapidity-1))
        if [ $rapidity -lt 0 ]
        then
            fun_erase_last_5
            fun_new_miam
            rapidity=39
        fi
    fi

    if [ $countEffect -eq 4 ]; then fun_effect_explosion 0; fi
    if [ $countEffect -lt 4 ]
    then
        if [ $latEffect -lt 4 ]
        then
            latEffect=$(($latEffect+1))
        else
            fun_effect_explosion 1
            countEffect=$(($countEffect+1))
            latEffect=1
        fi
    fi

    if [ $mode -ne 5 ] && [ $wall -eq 0 ]
    then
        if [ $pos1Y -lt $itemPosY ]; then fun_down 1
        elif [ $pos1X -gt $itemPosX ]; then fun_left 1
        elif [ $pos1Y -gt $itemPosY ]; then fun_up 1
        elif [ $pos1X -lt $itemPosX ]; then fun_right 1
        elif [ $mode -ne 5 ]; then point1=$(($point1+1)); colorWin=$colorp1; fun_new_miam
        fi

        if [ $pos2X -lt $itemPosX ]; then fun_right 2
        elif [ $pos2Y -lt $itemPosY ]; then fun_down 2
        elif [ $pos2Y -gt $itemPosY ]; then fun_up 2
        elif [ $pos2X -gt $itemPosX ]; then fun_left 2
        elif [ $mode -ne 5 ]; then point2=$(($point2+1)); colorWin=$colorp2; fun_new_miam
        fi

        if [ $pos3Y -lt $itemPosY ]; then fun_down 3
        elif [ $pos3X -gt $itemPosX ]; then fun_left 3
        elif [ $pos3X -lt $itemPosX ]; then fun_right 3
        elif [ $pos3Y -gt $itemPosY ]; then fun_up 3
        elif [ $mode -ne 5 ]; then point3=$(($point3+1)); colorWin=$colorp3; fun_new_miam
        fi

        if [ $pos4X -lt $itemPosX ]; then fun_right 4
        elif [ $pos4Y -gt $itemPosY ]; then fun_up 4
        elif [ $pos4X -gt $itemPosX ]; then fun_left 4
        elif [ $pos4Y -lt $itemPosY ]; then fun_down 4
        elif [ $mode -ne 5 ]; then point4=$(($point4+1)); colorWin=$colorp4; fun_new_miam
        fi
    elif [ $mode -ne 5 ] && [ $wall -eq 1 ]
    then
        if [ $pos1Y -lt $itemPosY ] && [ $tab[$(($pos1Y+1))*$colxx+$pos1X] == ' ' ]; then fun_down 1
        elif [ $pos1X -gt $itemPosX ] && [ $tab[$(($pos1Y))*$colxx+$(($pos1X-1))] == ' ' ]; then fun_left 1
        elif [ $pos1Y -gt $itemPosY ] && [ $tab[$(($pos1Y-1))*$colxx+$(($pos1X))] == ' ' ]; then fun_up 1
        elif [ $pos1X -lt $itemPosX ] && [ $tab[$(($pos1Y))*$colxx+$(($pos1X+1))] == ' ' ]; then fun_right 1
        elif [ $mode -ne 5 ] && [ $pos1X -eq $itemPosX ] && [ $pos1Y -eq $itemPosY ]; then point1=$(($point1+1)); colorWin=$colorp1; fun_new_miam
        fi

        if [ $pos2X -lt $itemPosX ] && [ $tab[$(($pos2Y))*$colxx+$(($pos2X+1))] == ' ' ]; then fun_right 2
        elif [ $pos2Y -lt $itemPosY ] && [ $tab[$(($pos2Y+1))*$colxx+$(($pos2X))] == ' ' ]; then fun_down 2
        elif [ $pos2Y -gt $itemPosY ] && [ $tab[$(($pos2Y-1))*$colxx+$(($pos2X))] == ' ' ]; then fun_up 2
        elif [ $pos2X -gt $itemPosX ] && [ $tab[$(($pos2Y))*$colxx+$(($pos2X-1))] == ' ' ]; then fun_left 2
        elif [ $mode -ne 5 ] && [ $pos2X -eq $itemPosX ] && [ $pos2Y -eq $itemPosY ]; then point2=$(($point2+1)); colorWin=$colorp2; fun_new_miam
        fi

        if [ $pos3Y -lt $itemPosY ] && [ $tab[$(($pos3Y+1))*$colxx+$(($pos3X))] == ' ' ]; then fun_down 3
        elif [ $pos3X -gt $itemPosX ] && [ $tab[$(($pos3Y))*$colxx+$(($pos3X-1))] == ' ' ]; then fun_left 3
        elif [ $pos3X -lt $itemPosX ] && [ $tab[$(($pos3Y))*$colxx+$(($pos3X+1))] == ' ' ]; then fun_right 3
        elif [ $pos3Y -gt $itemPosY ] && [ $tab[$(($pos3Y-1))*$colxx+$(($pos3X))] == ' ' ]; then fun_up 3
        elif [ $mode -ne 5 ] && [ $pos3X -eq $itemPosX ] && [ $pos3Y -eq $itemPosY ]; then point3=$(($point3+1)); colorWin=$colorp3; fun_new_miam
        fi

        if [ $pos4X -lt $itemPosX ] && [ $tab[$(($pos4Y))*$colxx+$(($pos4X+1))] == ' ' ]; then fun_right 4
        elif [ $pos4Y -gt $itemPosY ] && [ $tab[$(($pos4Y-1))*$colxx+$(($pos4X))] == ' ' ]; then fun_up 4
        elif [ $pos4X -gt $itemPosX ] && [ $tab[$(($pos4Y))*$colxx+$(($pos4X-1))] == ' ' ]; then fun_left 4
        elif [ $pos4Y -lt $itemPosY ] && [ $tab[$(($pos4Y+1))*$colxx+$(($pos4X))] == ' ' ]; then fun_down 4
        elif [ $mode -ne 5 ] && [ $pos4X -eq $itemPosX ] && [ $pos4Y -eq $itemPosY ]; then point4=$(($point4+1)); colorWin=$colorp4; fun_new_miam
        fi
    else
        fun_run_away_1
        fun_run_away_2
        fun_run_away_3
        fun_run_away_4
    fi

    if [ $point1 -gt $score ]
    then
        midX=$(($col/2-20))
        midY=$(($lin/2))
        tput cup $midY $midX
        echo -n 'THE WINNER IS '$colorp1$p1$null'. CONGRATULATIONS'
        midY=$(($midY+2))
        fun_time 5 $colorp1
    elif [ $point2 -gt $score ]
    then
        midX=$(($col/2-20))
        midY=$(($lin/2))
        tput cup $midY $midX
        echo -n 'THE WINNER IS '$colorp2$p2$null'. CONGRATULATIONS'
        midY=$(($midY+2))
        fun_time 5 $colorp2
    elif [ $point3 -gt $score ]
    then
        midX=$(($col/2-20))
        midY=$(($lin/2))
        tput cup $midY $midX
        echo -n 'THE WINNER IS '$colorp3$p3$null'. CONGRATULATIONS'
        midY=$(($midY+2))
        fun_time 5 $colorp3
    elif [ $point4 -gt $score ]
    then
        midX=$(($col/2-20))
        midY=$(($lin/2))
        tput cup $midY $midX
        echo -n 'THE WINNER IS '$colorp4$p4$null'. CONGRATULATIONS'
        midY=$(($midY+2))
        fun_time 5 $colorp4
    fi
done
