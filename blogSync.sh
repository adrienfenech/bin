#!/bin/bash
shopt -s xpg_echo

# This script is a snake like
# Version 0.6

# Variables

colorp1='\033[0;31m'
colorp2='\033[0;33m'
colorp3='\033[0;36m'
colorp4='\033[0;34m'
colorSuccess='\033[0;32m'
colorSpecial='\033[0;35m'
colorNormal='\033[0;36m'
colorErr='\033[0;31m'
null='\033[0m'

blogDir='~/blog/'
myBlogDir='~/my-blog/'
postBlogDir='~/my-blog/source/_posts/'
imagesBlogDir='~/my-blog/source/images/'


fun_print_log() {
    echo -e $colorNormal" "$1"."
}
fun_print_success() {
    echo -e $colorSuccess" "$1"."
}
fun_print_error() {
    echo -e $colorErr" "$1"."
}

fun_move_blog_to_my_blog() {
    for file in ~/blog/*
    do
        if [[ ${file: -4} == ".gif" ] || [ ${file: -4} == ".png" ] || [ ${file: -4} == ".jpg" ]]
            fun_print_log "copy of "$file" to "$imagesBlogDir
            cp $file $imagesBlogDir
        else
            fun_print_log "copy of "$file" to "$postBlogDir
            cp $file $postBlogDir
        fi
    done
    fun_print_success "Copy step DONE"
}

fun_move_blog_to_my_blog
cd $myBlogDir
hexo generate
fun_print_success "Generate step DONE"
cp -rf public/* /var/www/
fun_print_success "Upload step DONE"
echo " "
fun_print_success "All steps DONE"
