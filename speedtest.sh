#!/bin/bash
shopt -s xpg_echo

colorSuccess='\033[0;32m'
colorSpecial='\033[0;33m'
colorNormal='\033[0;36m'
colorErr='\033[0;31m'
null='\033[0m'

col=`tput cols`
lin=`tput lines`
bar_l=1
bar="-"

while [ 1 ]
do
    clear
    echo $colorNormal'New '$colorSpecial'ping'$colorNormal' testing in progress...'$null
    ping -c 5 -w 10 -q google.fr
    if [ $? -ne 0 ]
    then
        echo $colorErr'Ping on '$colorSpecial'google.fr'$colorErr' : FAILED'
        echo 'Please, check your connection and try again.'$null
    else
        echo $colorSuccess'Ping on '$colorSpecial'google.fr'$colorSuccess' : SUCCESS'$null
    fi
    echo ''
    echo $colorNormal'New '$colorSpecial'speed'$colorNormal' testing in progress...'$null
    speedtest-cli --simple
    if [ $? -ne 0 ]
    then
        echo $colorErr'Speedtest : FAILED'
        echo 'Please, check your connection and try again.'$null
    else
        echo $colorSuccess'Speedtest : SUCCESS'$null
    fi
    echo ''
    rab=30
    tput sc
    bool=1
    bar_l=1
    while [ $rab -gt 0 ]
    do
    tput cuu 1
    tput hpa 0
    tput el1
    echo $colorNormal'Next try in '$rab' seconds'$null
    tput rc
    sleep 1 &
    rab=$(($rab-1))
    pid=$!
    while kill -0 $pid 2> /dev/null
    do
        col=`tput cols`
        lin=`tput lines`
        bar="-"
        tput civis
        while [ $bar_l -lt $col ] && kill -0 $pid 2> /dev/null && [ $bool -eq 1 ]
        do
            echo -n '-['
            tput cub 1
            #bar="]$bar"
            bar_l=$(($bar_l+1))
            sleep 0.025
        done
        if kill -0 $pid 2> /dev/null && [ $bool -eq 1 ]; then 
        bar_l=$(($bar_l-1))
        tput cub 2
        tput el1
        tput cuf 2
        bool=2
        else tput sc; fi
        col=`tput cols`
        lin=`tput lines`
        while [ $bar_l -gt 0 ] && kill -0 $pid 2> /dev/null && [ $bool -eq 2 ]
        do
            tput cub 3
            echo -n ']-'
            bar="$bar"
            bar_l=$(($bar_l-1))
            sleep 0.02
        done
        if kill -0 $pid 2> /dev/null && [ $bool -eq 2 ]; then
        tput el
        bool=1
        else tput sc; fi
    done
    #tput el
    #tput cnorm
    done
    echo ''
done
